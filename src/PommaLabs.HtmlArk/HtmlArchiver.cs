﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using PommaLabs.HtmlArk.Core;
using PommaLabs.HtmlArk.Resources;
using PommaLabs.HtmlArk.Services.File;
using PommaLabs.HtmlArk.Services.Html;
using PommaLabs.HtmlArk.Services.Html.AngleSharp;
using PommaLabs.MimeTypes;

namespace PommaLabs.HtmlArk;

/// <summary>
///   Standard HTML archiver implementation.
/// </summary>
public sealed class HtmlArchiver : IHtmlArchiver
{
    private static readonly IFileService s_defaultFileService = new SystemFileService();
    private static readonly IHtmlService s_defaultHtmlService = new AngleSharpHtmlService(NullLogger<AngleSharpHtmlService>.Instance);

    private readonly IFileService _fileService;
    private readonly IHtmlService _htmlService;
    private readonly ILogger<HtmlArchiver> _logger;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="fileService">File service.</param>
    /// <param name="htmlService">HTML service.</param>
    public HtmlArchiver(
        ILogger<HtmlArchiver>? logger = null,
        IFileService? fileService = null,
        IHtmlService? htmlService = null)
    {
        _logger = logger ?? NullLogger<HtmlArchiver>.Instance;
        _fileService = fileService ?? s_defaultFileService;
        _htmlService = htmlService ?? s_defaultHtmlService;
    }

    /// <inheritdoc/>
    public async Task<string> ArchiveAsync(Uri htmlUri, HtmlArchiverSettings settings)
    {
        using var htmlDoc = await LoadHtmlDocumentAsync(htmlUri).ConfigureAwait(false);

        var resources = new Queue<IResource>();
        CollectAllAudios(htmlDoc, settings, resources);
        CollectAllImages(htmlDoc, settings, resources);
        CollectAllScripts(htmlDoc, settings, resources);
        CollectAllStyleSheets(htmlDoc, settings, resources);
        CollectAllVideos(htmlDoc, settings, resources);

        using var httpClient = CreateHttpClient(htmlDoc, settings);
        await EmbedExternalResourcesAsync(htmlDoc, settings, httpClient, resources).ConfigureAwait(false);

        return GenerateOutputHtml(htmlDoc, settings);
    }

    /// <inheritdoc/>
    public Task<string> ArchiveAsync(Uri htmlUri) => ArchiveAsync(htmlUri, HtmlArchiverSettings.Default);

    /// <inheritdoc/>
    public Task<string> ArchiveAsync(string htmlPath, HtmlArchiverSettings settings)
    {
        // Preconditions
        if (string.IsNullOrWhiteSpace(htmlPath)) throw new ArgumentException(ErrorMessages.HtmlArchiver_HtmlPathNullOrWhiteSpace, nameof(htmlPath));

        return ArchiveAsync(new Uri(Path.GetFullPath(htmlPath)), settings);
    }

    /// <inheritdoc/>
    public Task<string> ArchiveAsync(string htmlPath)
    {
        // Preconditions
        if (string.IsNullOrWhiteSpace(htmlPath)) throw new ArgumentException(ErrorMessages.HtmlArchiver_HtmlPathNullOrWhiteSpace, nameof(htmlPath));

        return ArchiveAsync(new Uri(Path.GetFullPath(htmlPath)));
    }

    private void CollectAllAudios(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (settings.IgnoreAudios)
        {
            _logger.LogTrace("Ignore audios flag is set to true, no audio will be collected");
            return;
        }
        foreach (var collectedResource in _htmlService.CollectAllAudios(htmlDoc, settings.IgnoreErrors))
        {
            resources.Enqueue(collectedResource);
        }
    }

    private void CollectAllImages(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (settings.IgnoreImages)
        {
            _logger.LogTrace("Ignore images flag is set to true, no image will be collected");
            return;
        }
        foreach (var collectedResource in _htmlService.CollectAllImages(htmlDoc, settings.IgnoreErrors))
        {
            resources.Enqueue(collectedResource);
        }
    }

    private void CollectAllScripts(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (settings.IgnoreJs)
        {
            _logger.LogTrace("Ignore JS flag is set to true, no scripts will be collected");
            return;
        }
        foreach (var collectedResource in _htmlService.CollectAllScripts(htmlDoc, settings.IgnoreErrors))
        {
            resources.Enqueue(collectedResource);
        }
    }

    private void CollectAllStyleSheets(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (settings.IgnoreCss)
        {
            _logger.LogTrace("Ignore CSS flag is set to true, no style sheet will be collected");
            return;
        }
        foreach (var collectedResource in _htmlService.CollectAllStyleSheets(htmlDoc, settings.IgnoreErrors))
        {
            resources.Enqueue(collectedResource);
        }
    }

    private void CollectAllVideos(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (settings.IgnoreVideos)
        {
            _logger.LogTrace("Ignore videos flag is set to true, no video will be collected");
            return;
        }
        foreach (var collectedResource in _htmlService.CollectAllVideos(htmlDoc, settings.IgnoreErrors))
        {
            resources.Enqueue(collectedResource);
        }
    }

    private HttpClient CreateHttpClient(IHtmlDocument htmlDoc, HtmlArchiverSettings settings)
    {
        _logger.LogTrace("HTTP client max response content buffer size will be set to: {HttpClientMaxResponseContentBufferSize}", settings.HttpClientMaxResourceSizeInBytes);
        _logger.LogTrace("HTTP client timeout will be set to: {HttpClientTimeout}", settings.HttpClientTimeout);

        var httpClient = new HttpClient
        {
            MaxResponseContentBufferSize = settings.HttpClientMaxResourceSizeInBytes,
            Timeout = settings.HttpClientTimeout
        };

        if (_htmlService.TryGetBaseAddressForHttpClient(htmlDoc, out var baseAddress))
        {
            _logger.LogTrace("HTTP client base address will be set to: {HttpClientBaseAddress}", baseAddress);
            httpClient.BaseAddress = baseAddress;
        }

        return httpClient;
    }

    private async Task EmbedExternalResourcesAsync(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, HttpClient httpClient, Queue<IResource> resources)
    {
        while (resources.Count > 0)
        {
            var resource = resources.Dequeue();
            try
            {
                switch (resource)
                {
                    case IExternalResource externalResource:
                        await ReadExternalResourceAsync(htmlDoc, httpClient, settings, externalResource).ConfigureAwait(false);
                        break;
                }

                if (resource.Contents.Length == 0) continue;
                resource.EmbedContents();
                resource.CollectNestedResources(_logger, settings, resources);
            }
            catch (Exception ex)
            {
                if (settings.IgnoreErrors)
                {
                    _logger.LogWarning(ex, "An error occurred while embedding following resource: {Resource}", resource);
                }
                else
                {
                    _logger.LogCritical(ex, "A critical error occurred while embedding following resource: {Resource}", resource);
                    throw;
                }
            }
        }
    }

    private string GenerateOutputHtml(IHtmlDocument htmlDoc, HtmlArchiverSettings settings)
    {
        return _htmlService.ConvertToHtml(htmlDoc, settings.Minify);
    }

    private async Task<IHtmlDocument> LoadHtmlDocumentAsync(Uri htmlUri)
    {
        try
        {
            IHtmlDocument htmlDoc;
            if (htmlUri.IsFile)
            {
                _logger.LogTrace("Specified HTML URI is a local file: {HtmlUri}", htmlUri);

                var htmlPath = htmlUri.LocalPath;
                if (!await _fileService.FileExistsAsync(htmlPath).ConfigureAwait(false))
                {
                    throw new FileNotFoundException(ErrorMessages.HtmlArchiver_HtmlFileNotFound, htmlPath);
                }

                // Directory exists because file exists and because we handle absolute paths.
                var htmlDirectory = Path.GetDirectoryName(htmlPath)!;

                var stopwatch = Stopwatch.StartNew();
                var htmlContents = await _fileService.ReadTextFileAsync(htmlPath).ConfigureAwait(false);
                htmlDoc = await _htmlService.OpenLocalHtmlDocumentAsync(htmlContents, htmlDirectory).ConfigureAwait(false);

                _logger.LogTrace("Local HTML document has been loaded in {ElapsedMilliseconds} msec", stopwatch.ElapsedMilliseconds);
            }
            else
            {
                _logger.LogTrace("Specified HTML URI is a remote URL: {HtmlUri}", htmlUri);

                var stopwatch = Stopwatch.StartNew();
                htmlDoc = await _htmlService.OpenRemoteHtmlDocumentAsync(htmlUri).ConfigureAwait(false);

                _logger.LogTrace("Remote HTML document has been loaded in {ElapsedMilliseconds} msec", stopwatch.ElapsedMilliseconds);
            }
            htmlDoc = _htmlService.StripUnnecessaryElements(htmlDoc);
            return _htmlService.AppendContentSecurityPolicyMetaElement(htmlDoc);
        }
        catch (Exception ex)
        {
            ex.Data["HtmlUri"] = htmlUri;
            throw;
        }
    }

    private async Task ReadExternalResourceAsync(IHtmlDocument htmlDoc, HttpClient httpClient, HtmlArchiverSettings settings, IExternalResource externalResource)
    {
        var externalResourceUri = externalResource.Uri;
        var exceptions = new List<Exception>();

        // Remote resource:
        if ((externalResourceUri.IsAbsoluteUri && UriHelper.IsHttpScheme(externalResourceUri)) || httpClient.BaseAddress != null)
        {
            try
            {
                if (await TryDownloadRemoteResourceAsync(httpClient, settings, externalResource).ConfigureAwait(false))
                {
                    // Remote resource has been correctly downloaded.
                    return;
                }
            }
            catch (Exception ex) // Exception is collected and thrown later, if necessary.
            {
                exceptions.Add(ex);
            }
        }

        // Local resource:
        if (!externalResourceUri.IsAbsoluteUri || externalResourceUri.IsFile)
        {
            try
            {
                if (TryReadLocalResource(htmlDoc, settings, externalResource))
                {
                    // Local resource has been correctly read.
                    return;
                }
            }
            catch (Exception ex) // Exception is collected and thrown later, if necessary.
            {
                exceptions.Add(ex);
            }
        }

        // Unknown file source:
        if (settings.IgnoreErrors)
        {
            _logger.LogError("An error occurred while reading external resource. It was neither a remote URI nor a local path: {ExternalResourceUri}", externalResourceUri);
            return;
        }
        if (exceptions.Count > 0)
        {
            throw new AggregateException(ErrorMessages.HtmlArchiver_UnreadableExternalResource, exceptions);
        }
        _logger.LogCritical("A critical error occurred while reading external resource. It was neither a remote URI nor a local path: {ExternalResourceUri}", externalResourceUri);
        throw new InvalidOperationException($"External resource could not be read, it was neither a remote URI nor a local path: {externalResourceUri}");
    }

    private async Task<bool> TryDownloadRemoteResourceAsync(HttpClient httpClient, HtmlArchiverSettings settings, IExternalResource externalResource)
    {
        var externalResourceUri = externalResource.Uri!;

        if (externalResourceUri.IsAbsoluteUri && !UriHelper.IsHttpScheme(externalResourceUri))
        {
            externalResourceUri = new Uri(externalResourceUri.OriginalString, UriKind.Relative);
        }

        _logger.LogTrace("Downloading remote resource with absolute URI: {ExternalResourceUri}", externalResourceUri);
        using var response = await httpClient.GetAsync(externalResourceUri).ConfigureAwait(false);

        if (!response.IsSuccessStatusCode)
        {
            if (settings.IgnoreErrors)
            {
                _logger.LogError("An error occurred while downloading remote resource. HTTP response status code was: {ResponseStatusCode}", response.StatusCode);
                return false;
            }
            _logger.LogCritical("A critical error occurred while downloading remote resource. HTTP response status code was: {ResponseStatusCode}", response.StatusCode);
            throw new InvalidOperationException($"Remote resource download failed, HTTP response status code was: {response.StatusCode}");
        }

        externalResource.CharSet = response.Content.Headers.ContentType?.CharSet?.ToUpperInvariant() ?? string.Empty;
        externalResource.FileName = response.Content.Headers.ContentDisposition?.FileName?.Trim('\"') ?? UriHelper.GetFileName(externalResourceUri) ?? string.Empty;

        externalResource.ContentType = response.Content.Headers.ContentType?.MediaType ?? string.Empty;
        externalResource.Contents = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);

        using var externalResourceStream = new MemoryStream(externalResource.Contents);
        if (string.Equals(externalResource.ContentType, MimeTypeMap.TEXT.CSS))
        {
            _logger.LogTrace("Response content type is a text file specialization: {ExternalResourceContentType}", externalResource.ContentType);
        }
        else if (MimeTypeMap.TryGetMimeType(externalResourceStream, externalResource.FileName, out var contentType) && !string.Equals(externalResource.ContentType, contentType, StringComparison.OrdinalIgnoreCase))
        {
            _logger.LogTrace("Response content type has been overridden. It was \"{OriginalResourceContentType}\", now it is \"{OverriddenResourceContentType}\"", externalResource.ContentType, contentType);
            externalResource.ContentType = contentType;
        }
        else if (string.IsNullOrWhiteSpace(externalResource.ContentType))
        {
            externalResource.ContentType = MimeTypeMap.APPLICATION.OCTET_STREAM;
            _logger.LogTrace("Response content type could not be read from response headers and it could not be deduced. It has been set to: {ExternalResourceContentType}", externalResource.ContentType);
        }
        else
        {
            _logger.LogTrace("Response content type read from response headers: {ExternalResourceContentType}", externalResource.ContentType);
        }

        return true;
    }

    private bool TryReadLocalResource(IHtmlDocument htmlDoc, HtmlArchiverSettings settings, IExternalResource externalResource)
    {
        var externalResourceUri = externalResource.Uri!;
        var rootPath = htmlDoc.DocumentUri.LocalPath;

        _logger.LogTrace("Reading local resource inside root path: {RootPath}", rootPath);

        string localResourcePath;
        if (externalResourceUri.IsAbsoluteUri)
        {
            localResourcePath = externalResourceUri.LocalPath;
        }
        else
        {
            localResourcePath = Path.Combine(rootPath, UriHelper.UnescapeDataString(externalResourceUri.OriginalString));
        }

        _logger.LogTrace("Reading local resource with path: {ExternalResourceUri}", localResourcePath);

        if (!localResourcePath.StartsWith(rootPath, StringComparison.OrdinalIgnoreCase))
        {
            if (settings.IgnoreErrors)
            {
                _logger.LogError("An error occurred while reading local resource. File path is not a subpath of HTML file path: {ExternalResourceUri}", localResourcePath);
                return false;
            }
            _logger.LogCritical("A critical error occurred while reading local resource. File path is not a subpath of HTML file path: {ExternalResourceUri}", localResourcePath);
            throw new InvalidOperationException($"Local resource copy failed, file path is not a subpath of HTML file path: {localResourcePath}");
        }

        if (!File.Exists(localResourcePath))
        {
            if (settings.IgnoreErrors)
            {
                _logger.LogError("An error occurred while reading local resource. File path does not exist: {ExternalResourceUri}", localResourcePath);
                return false;
            }
            _logger.LogCritical("A critical error occurred while reading local resource. File path does not exist: {ExternalResourceUri}", localResourcePath);
            throw new InvalidOperationException($"Local resource copy failed, file path does not exist: {localResourcePath}");
        }

        try
        {
            externalResource.FileName = Path.GetFileName(localResourcePath);
            externalResource.Contents = File.ReadAllBytes(localResourcePath);
        }
        catch (Exception ex)
        {
            ex.Data["ExternalResourceContentType"] = externalResource.ContentType;
            ex.Data["ExternalResourceFileName"] = externalResource.FileName;
            ex.Data["ExternalResourceUri"] = externalResource.Uri;
            if (!settings.IgnoreErrors)
            {
                throw;
            }
            _logger.LogError(ex, "An error occurred while reading local resource");
            return false;
        }

        using var externalResourceStream = new MemoryStream(externalResource.Contents);
        externalResource.ContentType = MimeTypeMap.GetMimeType(externalResourceStream, externalResource.FileName, throwIfMissing: false);
        _logger.LogTrace("File content type has been automatically detected: {ExternalResourceContentType}", externalResource.ContentType);

        return true;
    }
}
