﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.HtmlArk;

/// <summary>
///   Settings which can be used to customize the HTML archival process.
/// </summary>
public sealed class HtmlArchiverSettings
{
    /// <summary>
    ///   Default settings.
    /// </summary>
    public static HtmlArchiverSettings Default { get; } = new();

    /// <summary>
    ///   How many bytes can be downloaded for each resource. Defaults to 50MB.
    /// </summary>
    public long HttpClientMaxResourceSizeInBytes { get; set; } = 50 * 1024 * 1024;

    /// <summary>
    ///   Timeout of the internal HTTP client. Defaults to 30 seconds.
    /// </summary>
    public TimeSpan HttpClientTimeout { get; set; } = TimeSpan.FromSeconds(30);

    /// <summary>
    ///   Ignores audios during archival. Defaults to false.
    /// </summary>
    public bool IgnoreAudios { get; set; }

    /// <summary>
    ///   Ignores style sheets during archival. Defaults to false.
    /// </summary>
    public bool IgnoreCss { get; set; }

    /// <summary>
    ///   Ignores unreadable resources. Defaults to false.
    /// </summary>
    public bool IgnoreErrors { get; set; }

    /// <summary>
    ///   Ignores fonts during archival. Defaults to false.
    /// </summary>
    public bool IgnoreFonts { get; set; }

    /// <summary>
    ///   Ignores images during archival. Defaults to false.
    /// </summary>
    public bool IgnoreImages { get; set; }

    /// <summary>
    ///   Ignores external JavaScript during archival. Defaults to false.
    /// </summary>
    public bool IgnoreJs { get; set; }

    /// <summary>
    ///   Ignores videos during archival. Defaults to false.
    /// </summary>
    public bool IgnoreVideos { get; set; }

    /// <summary>
    ///   Minifies output HTML. Defaults to false.
    /// </summary>
    public bool Minify { get; set; }
}
