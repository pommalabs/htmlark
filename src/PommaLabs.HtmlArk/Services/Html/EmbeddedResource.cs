﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace PommaLabs.HtmlArk.Services.Html;

internal sealed class EmbeddedResource : IResource
{
    public EmbeddedResource(IHtmlNodeWrapper htmlNode)
    {
        HtmlNode = htmlNode is ITextNodeWrapper textNode ? htmlNode : throw new ArgumentException("Embedded resources need TEXT node wrappers", nameof(htmlNode));

        // Load contents from wrapped TEXT node, since it already contains what is needed.
        Contents = Encoding.UTF8.GetBytes(textNode.Contents);
        ContentType = textNode.ContentType;
    }

    public string CharSet { get; set; } = string.Empty;

    public byte[] Contents { get; set; } = Array.Empty<byte>();

    public string ContentType { get; set; } = string.Empty;

    public IHtmlNodeWrapper HtmlNode { get; }

    public void CollectNestedResources(ILogger logger, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (HtmlNode is ITextNodeWrapper textNode)
        {
            foreach (var uri in textNode.CollectNestedResourceUris(logger, settings, Encoding.UTF8.GetString(Contents)))
            {
                resources.Enqueue(new NestedExternalResource(this, uri));
            }
        }
    }

    public void EmbedContents()
    {
        HtmlNode.EmbedContents(Encoding.UTF8.GetString(Contents));
    }
}
