﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace PommaLabs.HtmlArk.Services.Html;

internal interface ITextNodeWrapper : IHtmlNodeWrapper
{
    string Contents { get; }

    string ContentType { get; }

    HashSet<Uri> CollectNestedResourceUris(ILogger logger, HtmlArchiverSettings settings, string resourceContents);
}
