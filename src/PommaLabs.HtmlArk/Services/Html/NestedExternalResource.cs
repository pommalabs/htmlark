﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Extensions.Logging;
using PommaLabs.HtmlArk.Core;

namespace PommaLabs.HtmlArk.Services.Html;

[DebuggerDisplay("Uri = {Uri}")]
internal sealed class NestedExternalResource : IExternalResource
{
    private static readonly char[] s_removeQueryStringAndHashSplitChars = new[] { '?', '#' };

    private readonly Uri _rawSourceUri;

    public NestedExternalResource(IResource parentResource, Uri uri)
    {
        ParentResource = parentResource;
        _rawSourceUri = uri;

        // Nested relative URIs are relative to parent URI, not to page URI: therefore, they
        // must be rebuilt using the parent URI as base. However, if parent URI does not exist,
        // it means that we are handling an embedded style sheet and given URI should be
        // converted into a local file URI.
        if (uri.IsAbsoluteUri)
        {
            Uri = uri;
        }
        else
        {
            var parentHrefNode = ParentResource.HtmlNode as IHrefNodeWrapper;
            if (parentHrefNode?.ParsedSourceUri == null)
            {
                // Parent node is embedded.
                Uri = RemoveQueryStringAndHash(uri);
            }
            else
            {
                Uri = new Uri(parentHrefNode.ParsedSourceUri, uri);
            }
        }
    }

    public string CharSet { get; set; } = string.Empty;

    public byte[] Contents { get; set; } = Array.Empty<byte>();

    public string ContentType { get; set; } = string.Empty;

    public string FileName { get; set; } = string.Empty;

    public IHtmlNodeWrapper HtmlNode => ParentResource.HtmlNode;

    public IResource ParentResource { get; }

    public Uri Uri { get; }

    public void CollectNestedResources(ILogger logger, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        // Nothing to do here.
    }

    public void EmbedContents()
    {
        var dataUri = UriHelper.BuildDataUri(Contents, ContentType, CharSet);

        var externalResourceString = Encoding.UTF8.GetString(ParentResource.Contents);
        externalResourceString = externalResourceString.Replace(_rawSourceUri.ToString(), dataUri);

        ParentResource.Contents = Encoding.UTF8.GetBytes(externalResourceString);
        ParentResource.EmbedContents();
    }

    private static Uri RemoveQueryStringAndHash(Uri uri)
    {
        // Preconditions
        Debug.Assert(!uri.IsAbsoluteUri);

        return new Uri(uri.OriginalString.Split(s_removeQueryStringAndHashSplitChars)[0], UriKind.Relative);
    }
}
