﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.HtmlArk.Services.Html;

/// <summary>
///   Wraps an HTML node.
/// </summary>
public interface IHtmlNodeWrapper
{
    /// <summary>
    ///   Embeds specified contents into the wrapped HTML node.
    /// </summary>
    /// <param name="resourceContents">Resource contents.</param>
    void EmbedContents(string resourceContents);
}
