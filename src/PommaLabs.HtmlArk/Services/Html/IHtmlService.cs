﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PommaLabs.HtmlArk.Services.Html;

/// <summary>
///   Interface for HTML manipulation.
/// </summary>
public interface IHtmlService
{
    /// <summary>
    ///   Appends a new meta element with "Content-Security-Policy" http-equiv.
    ///   Policy will allow local protocols for every type of resource.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <returns>An HTML document with a meta element containing a "Content-Security-Policy".</returns>
    IHtmlDocument AppendContentSecurityPolicyMetaElement(IHtmlDocument htmlDoc);

    /// <summary>
    ///   Collects all audios contained into specified HTML document.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="ignoreErrors">
    ///   Determines whether an exception is thrown when a resource cannot be collected.
    /// </param>
    /// <returns>All collected resources.</returns>
    IEnumerable<IResource> CollectAllAudios(IHtmlDocument htmlDoc, bool ignoreErrors);

    /// <summary>
    ///   Collects all images contained into specified HTML document.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="ignoreErrors">
    ///   Determines whether an exception is thrown when a resource cannot be collected.
    /// </param>
    /// <returns>All collected resources.</returns>
    IEnumerable<IResource> CollectAllImages(IHtmlDocument htmlDoc, bool ignoreErrors);

    /// <summary>
    ///   Collects all scripts contained into specified HTML document.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="ignoreErrors">
    ///   Determines whether an exception is thrown when a resource cannot be collected.
    /// </param>
    /// <returns>All collected resources.</returns>
    IEnumerable<IResource> CollectAllScripts(IHtmlDocument htmlDoc, bool ignoreErrors);

    /// <summary>
    ///   Collects all images contained into specified HTML document.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="ignoreErrors">
    ///   Determines whether an exception is thrown when a resource cannot be collected.
    /// </param>
    /// <returns>All collected resources.</returns>
    IEnumerable<IResource> CollectAllStyleSheets(IHtmlDocument htmlDoc, bool ignoreErrors);

    /// <summary>
    ///   Collects all videos contained into specified HTML document.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="ignoreErrors">
    ///   Determines whether an exception is thrown when a resource cannot be collected.
    /// </param>
    /// <returns>All collected resources.</returns>
    IEnumerable<IResource> CollectAllVideos(IHtmlDocument htmlDoc, bool ignoreErrors);

    /// <summary>
    ///   Converts specified HTML document into a string.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="minify">Whether the output HTML should be minified or not.</param>
    /// <returns>An HTML string.</returns>
    string ConvertToHtml(IHtmlDocument htmlDoc, bool minify);

    /// <summary>
    ///   Opens a local HTML document, whose contents have been specified in
    ///   <paramref name="htmlContents"/> parameter.
    /// </summary>
    /// <param name="htmlContents">HTML contents.</param>
    /// <param name="htmlDirectory">Directory containing the HTML file.</param>
    /// <returns>An HTML document which can be searched.</returns>
    /// <exception cref="ArgumentException">HTML contents are null, empty or blank.</exception>
    Task<IHtmlDocument> OpenLocalHtmlDocumentAsync(string htmlContents, string htmlDirectory);

    /// <summary>
    ///   Opens a remote HTML document.
    /// </summary>
    /// <param name="htmlUri">HTML URI.</param>
    /// <returns>An HTML document which can be searched.</returns>
    /// <exception cref="ArgumentException">HTML URI is a file URI.</exception>
    Task<IHtmlDocument> OpenRemoteHtmlDocumentAsync(Uri htmlUri);

    /// <summary>
    ///   Removes all HTML elements which are not needed after the archival process.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <returns>An HTML document whose contents have been cleaned up.</returns>
    IHtmlDocument StripUnnecessaryElements(IHtmlDocument htmlDoc);

    /// <summary>
    ///   Gets the base address which will be used by the HTTP client in order to download resources.
    /// </summary>
    /// <param name="htmlDoc">HTML document.</param>
    /// <param name="baseAddress">Base address.</param>
    /// <returns>
    ///   <c>true</c> if the base address could be extracted from specified
    ///   <paramref name="htmlDoc"/>, <c>false</c> otherwise.
    /// </returns>
    bool TryGetBaseAddressForHttpClient(IHtmlDocument htmlDoc, out Uri? baseAddress);
}
