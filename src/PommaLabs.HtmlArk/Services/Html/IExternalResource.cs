﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.HtmlArk.Services.Html;

internal interface IExternalResource : IResource
{
    string FileName { get; set; }

    Uri Uri { get; }
}
