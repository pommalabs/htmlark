﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace PommaLabs.HtmlArk.Services.Html;

/// <summary>
///   Represents an HTML resource: an image, a video, a script, ...
/// </summary>
public interface IResource
{
    /// <summary>
    ///   Character set.
    /// </summary>
    string CharSet { get; set; }

    /// <summary>
    ///   Resource contents.
    /// </summary>
    byte[] Contents { get; set; }

    /// <summary>
    ///   Content type.
    /// </summary>
    string ContentType { get; set; }

    /// <summary>
    ///   Wrapped HTML node.
    /// </summary>
    IHtmlNodeWrapper HtmlNode { get; }

    /// <summary>
    ///   Adds nested resources into the resource queue.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="settings">Settings.</param>
    /// <param name="resources">Resource queue.</param>
    void CollectNestedResources(ILogger logger, HtmlArchiverSettings settings, Queue<IResource> resources);

    /// <summary>
    ///   Embeds resource contents into the wrapped HTML node.
    /// </summary>
    void EmbedContents();
}
