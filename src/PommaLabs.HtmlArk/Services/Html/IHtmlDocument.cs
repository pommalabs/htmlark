﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.HtmlArk.Services.Html;

/// <summary>
///   Represents an HTML document which can searched during the archival flow.
/// </summary>
public interface IHtmlDocument : IDisposable
{
    /// <summary>
    ///   Gets the URI of the current document.
    /// </summary>
    Uri DocumentUri { get; }
}
