﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Extensions.Logging;
using PommaLabs.HtmlArk.Core;

namespace PommaLabs.HtmlArk.Services.Html;

[DebuggerDisplay("Uri = {Uri}")]
internal sealed class ExternalResource : IExternalResource
{
    private readonly IHrefNodeWrapper _hrefNode;

    public ExternalResource(IHrefNodeWrapper htmlNode)
    {
        _hrefNode = htmlNode;
    }

    public string CharSet { get; set; } = string.Empty;

    public byte[] Contents { get; set; } = Array.Empty<byte>();

    public string ContentType { get; set; } = string.Empty;

    public string FileName { get; set; } = string.Empty;

    public IHtmlNodeWrapper HtmlNode => _hrefNode;

    public Uri Uri => _hrefNode.ParsedSourceUri!;

    public void CollectNestedResources(ILogger logger, HtmlArchiverSettings settings, Queue<IResource> resources)
    {
        if (HtmlNode is ITextNodeWrapper textNode)
        {
            foreach (var uri in textNode.CollectNestedResourceUris(logger, settings, Encoding.UTF8.GetString(Contents)))
            {
                resources.Enqueue(new NestedExternalResource(this, uri));
            }
        }
    }

    public void EmbedContents()
    {
        HtmlNode.EmbedContents(UriHelper.BuildDataUri(Contents, ContentType, CharSet));
    }
}
