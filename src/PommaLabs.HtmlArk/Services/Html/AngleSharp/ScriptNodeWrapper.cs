﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using AngleSharp.Html.Dom;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class ScriptNodeWrapper : HrefNodeWrapper<IHtmlScriptElement>
{
    public ScriptNodeWrapper(IHtmlScriptElement wrappedScriptNode)
        : base(wrappedScriptNode, AttributeNames.Source, wrappedScriptNode.Source)
    {
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the script data URI.
        WrappedHtmlNode.Source = resourceContents;
        WrappedHtmlNode.Integrity = string.Empty;
    }
}
