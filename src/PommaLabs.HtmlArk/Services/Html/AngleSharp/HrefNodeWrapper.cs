﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using AngleSharp;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal abstract class HrefNodeWrapper<TNode> : HtmlNodeWrapper<TNode>, IHrefNodeWrapper
    where TNode : IHtmlElement
{
    protected HrefNodeWrapper(TNode wrappedHtmlNode, string attributeName, string? rawSourceUri) : base(wrappedHtmlNode)
    {
        AttributeName = attributeName;
        RawSourceUri = rawSourceUri;
    }

    public Uri? ParsedSourceUri { get; private set; }

    public string? RawSourceUri { get; }

    protected string AttributeName { get; }

    public override bool CanBeCollected(ILogger logger)
    {
        if (string.IsNullOrWhiteSpace(RawSourceUri))
        {
            logger.LogWarning("Skipping {NodeName} node '{NodeHtml}', because its '{AttributeName}' attribute is missing, empty or blank", WrappedHtmlNode.NodeName, WrappedHtmlNode.ToHtml(), AttributeName);
            return false;
        }
        if (RawSourceUri!.StartsWith("data:", StringComparison.OrdinalIgnoreCase))
        {
            logger.LogWarning("Skipping {NodeName} node '{NodeHtml}', because its '{AttributeName}' attribute already contains a data URI", WrappedHtmlNode.NodeName, WrappedHtmlNode.ToHtml(), AttributeName);
            return false;
        }
        return true;
    }

    /// <summary>
    ///   Tries to parse raw source URI.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <returns><c>true</c> if raw source URI could be parsed, <c>false</c> otherwise.</returns>
    public bool TryParseRawSourceUri(ILogger logger)
    {
        if (Uri.TryCreate(RawSourceUri, UriKind.Absolute, out var absoluteUri))
        {
            logger.LogTrace("Source URI is valid and it is an absolute URI: {ParsedSourceUri}", absoluteUri);
            ParsedSourceUri = absoluteUri;
            return true;
        }
        if (Uri.TryCreate(RawSourceUri, UriKind.Relative, out var relativeUri))
        {
            logger.LogTrace("Source URI is valid and it is a relative URI: {ParsedSourceUri}", relativeUri);
            ParsedSourceUri = relativeUri;
            return true;
        }
        return false;
    }
}
