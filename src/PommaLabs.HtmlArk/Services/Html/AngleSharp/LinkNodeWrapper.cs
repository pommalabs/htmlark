﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using PommaLabs.MimeTypes;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class LinkNodeWrapper : HrefNodeWrapper<IHtmlLinkElement>, ITextNodeWrapper
{
    public LinkNodeWrapper(IHtmlLinkElement wrappedLinkNode)
        : base(wrappedLinkNode, AttributeNames.Href, wrappedLinkNode.Href)
    {
    }

    public string Contents => WrappedHtmlNode.Href!;

    public string ContentType => string.IsNullOrWhiteSpace(WrappedHtmlNode.Type) ? MimeTypeMap.TEXT.CSS : WrappedHtmlNode.Type!;

    public HashSet<Uri> CollectNestedResourceUris(ILogger logger, HtmlArchiverSettings settings, string resourceContents)
    {
        return WrappedHtmlNode.Relation != "stylesheet" ? new() : CollectNestedCssResourceUris(logger, settings, resourceContents);
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the style sheet data URI.
        WrappedHtmlNode.Href = resourceContents;
        WrappedHtmlNode.Integrity = string.Empty;
    }
}
