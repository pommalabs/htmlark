﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Linq;
using AngleSharp.Html.Dom;
using PommaLabs.HtmlArk.Resources;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class SourceNodeWrapper : HrefNodeWrapper<IHtmlSourceElement>
{
    public SourceNodeWrapper(IHtmlSourceElement wrappedSourceNode, string attributeName)
        : base(wrappedSourceNode, attributeName, GetRawSourceUri(wrappedSourceNode, attributeName))
    {
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the media data URI.
        switch (AttributeName)
        {
            case AttributeNames.Source:
                WrappedHtmlNode.Source = resourceContents;
                break;

            case AttributeNames.SourceSet:
                // Raw source URI is not null because that was already verified in
                // CanBeCollected method.
                WrappedHtmlNode.SourceSet = WrappedHtmlNode.SourceSet?.Replace(RawSourceUri!, resourceContents);
                break;
        }
    }

    private static string? GetRawSourceUri(IHtmlSourceElement wrappedSourceNode, string attributeName)
    {
        return attributeName switch
        {
            AttributeNames.Source => wrappedSourceNode.Source,
            AttributeNames.SourceSet => wrappedSourceNode.SourceSet?.Split(' ').FirstOrDefault(),
            _ => throw new ArgumentException(ErrorMessages.HtmlNodeWrapper_UnknownAttributeName, nameof(attributeName))
        };
    }
}
