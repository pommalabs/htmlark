﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using PommaLabs.HtmlArk.Core;
using PommaLabs.HtmlArk.Resources;
using PommaLabs.MimeTypes;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

/// <summary>
///   HTML service based on AngleSharp library.
/// </summary>
public sealed class AngleSharpHtmlService : IHtmlService
{
    private static readonly MinifyMarkupFormatter s_minifier = new();

    private readonly ILogger<AngleSharpHtmlService> _logger;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    public AngleSharpHtmlService(ILogger<AngleSharpHtmlService> logger)
    {
        _logger = logger;
    }

    /// <inheritdoc/>
    public IHtmlDocument AppendContentSecurityPolicyMetaElement(IHtmlDocument htmlDoc)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;

        // If Content-Security-Policy (CSP) meta element is set, it should be removed
        // in order to prevent embedded "data:" content from being blocked.
        var cspMetaElements = angleSharpDocument.Head
            ?.GetElementsByTagName("meta")
            .Cast<IHtmlMetaElement>()
            .Where(m => string.Equals(m.HttpEquivalent, "Content-Security-Policy"))
            .ToList() ?? new List<IHtmlMetaElement>();

        // It does not make sense to have multiple CSP meta elements, since you only need one.
        // However, a precautionary approach is used and all CSP meta elements are removed.
        _logger.LogTrace("{CspMetaElements} meta element with \"Content-Security-Policy\" http-equiv will be removed", cspMetaElements.Count);
        cspMetaElements.ForEach(m => m.Remove());

        // A new CSP meta element, allowing local protocols for every type of resource,
        // is added to head element if available.
        var cspMetaElement = angleSharpDocument.CreateElement<IHtmlMetaElement>();
        cspMetaElement.HttpEquivalent = "Content-Security-Policy";
        cspMetaElement.Content = "default-src 'unsafe-inline' data:";

        if (angleSharpDocument.Head != null)
        {
            _logger.LogTrace("New meta element with \"Content-Security-Policy\" http-equiv and \"{CspContent}\" content will be added", cspMetaElement.Content);
            angleSharpDocument.Head?.Append(cspMetaElement);
        }

        return htmlDoc;
    }

    /// <inheritdoc/>
    public IEnumerable<IResource> CollectAllAudios(IHtmlDocument htmlDoc, bool ignoreErrors)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;
        var counts = new CollectionCounts();

        return CollectAudios(ignoreErrors, angleSharpDocument, counts)
            .Concat(LogCollectionCounts("audios", counts));
    }

    /// <inheritdoc/>
    public IEnumerable<IResource> CollectAllImages(IHtmlDocument htmlDoc, bool ignoreErrors)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;
        var counts = new CollectionCounts();

        return CollectImages(ignoreErrors, angleSharpDocument, counts)
            .Concat(CollectImageSourceSets(ignoreErrors, angleSharpDocument, counts))
            .Concat(CollectPictureSources(ignoreErrors, angleSharpDocument, counts))
            .Concat(CollectVideoPosters(ignoreErrors, angleSharpDocument, counts))
            .Concat(CollectFavicons(ignoreErrors, angleSharpDocument, counts))
            .Concat(LogCollectionCounts("images", counts));
    }

    /// <inheritdoc/>
    public IEnumerable<IResource> CollectAllScripts(IHtmlDocument htmlDoc, bool ignoreErrors)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;
        var counts = new CollectionCounts();

        return CollectScripts(ignoreErrors, angleSharpDocument, counts)
            .Concat(LogCollectionCounts("scripts", counts));
    }

    /// <inheritdoc/>
    public IEnumerable<IResource> CollectAllStyleSheets(IHtmlDocument htmlDoc, bool ignoreErrors)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;
        var counts = new CollectionCounts();

        return CollectExternalStyleSheets(ignoreErrors, angleSharpDocument, counts)
            .Concat(CollectEmbeddedStyleSheets(angleSharpDocument, counts))
            .Concat(LogCollectionCounts("style sheets", counts));
    }

    /// <inheritdoc/>
    public IEnumerable<IResource> CollectAllVideos(IHtmlDocument htmlDoc, bool ignoreErrors)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;
        var counts = new CollectionCounts();

        return CollectVideos(ignoreErrors, angleSharpDocument, counts)
            .Concat(CollectVideoSources(ignoreErrors, angleSharpDocument, counts))
            .Concat(LogCollectionCounts("videos", counts));
    }

    /// <inheritdoc/>
    public string ConvertToHtml(IHtmlDocument htmlDoc, bool minify)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;

        return angleSharpDocument.ToHtml(minify ? s_minifier : HtmlMarkupFormatter.Instance);
    }

    /// <inheritdoc/>
    public Task<IHtmlDocument> OpenLocalHtmlDocumentAsync(string htmlContents, string htmlDirectory)
    {
        // Preconditions
        if (string.IsNullOrWhiteSpace(htmlContents)) throw new ArgumentException(ErrorMessages.HtmlService_HtmlContentsNullOrWhiteSpace, nameof(htmlContents));

        return OpenLocalHtmlDocumentInternalAsync(htmlContents, htmlDirectory);
    }

    /// <inheritdoc/>
    public Task<IHtmlDocument> OpenRemoteHtmlDocumentAsync(Uri htmlUri)
    {
        // Preconditions
        if (htmlUri.IsFile) throw new ArgumentException(ErrorMessages.HtmlService_HtmlUriIsFile, nameof(htmlUri));

        return OpenRemoteHtmlDocumentInternalAsync(htmlUri);
    }

    /// <inheritdoc/>
    public IHtmlDocument StripUnnecessaryElements(IHtmlDocument htmlDoc)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;

        // Remove links with "preload" relation, because linked resources are already embedded
        // as Base64 URIs. Moreover, by embedding the resource we would break the link.
        var preloadedLinks = angleSharpDocument
            .GetElementsByTagName("link")
            .Cast<IHtmlLinkElement>()
            .Where(l => string.Equals(l.Relation, "preload", StringComparison.OrdinalIgnoreCase))
            .ToList();

        _logger.LogTrace("{PrealoadedLinkCount} links with \"preload\" relation will be removed", preloadedLinks.Count);
        preloadedLinks.ForEach(l => l.Remove());

        return htmlDoc;
    }

    /// <inheritdoc/>
    public bool TryGetBaseAddressForHttpClient(IHtmlDocument htmlDoc, out Uri? baseAddress)
    {
        // Preconditions
        Debug.Assert(htmlDoc is HtmlDocument, ErrorMessages.HtmlService_HtmlDocumentWithUnexpectedType);

        var angleSharpDocument = ((HtmlDocument)htmlDoc).Document;

        var documentUri = new Uri(angleSharpDocument.DocumentUri);
        if (UriHelper.IsHttpScheme(documentUri) && documentUri.IsAbsoluteUri)
        {
            _logger.LogTrace("Document URI scheme is 'http' or 'https', it will be used as base address: {DocumentUri}", documentUri);
            baseAddress = new Uri(documentUri.GetLeftPart(UriPartial.Path));
            return true;
        }

        var baseUri = new Uri(angleSharpDocument.BaseUri);
        if (UriHelper.IsHttpScheme(baseUri) && baseUri.IsAbsoluteUri)
        {
            _logger.LogTrace("Base URI scheme is 'http' or 'https', it will be used as base address: {BaseUri}", baseUri);
            baseAddress = baseUri;
            return true;
        }

        _logger.LogTrace("No base address could be deduced from HTML document");
        baseAddress = null;
        return false;
    }

    private static IBrowsingContext CreateBrowsingContext()
    {
        return BrowsingContext.New(Configuration.Default.WithDefaultLoader());
    }

    private static async Task<IHtmlDocument> OpenLocalHtmlDocumentInternalAsync(string htmlContents, string htmlDirectory)
    {
        var htmlDirectoryUri = $"{new Uri(htmlDirectory).AbsoluteUri}/";

        var browsingContext = CreateBrowsingContext();
        var document = await browsingContext.OpenAsync(req => { req.Content(htmlContents); req.Address(htmlDirectoryUri); }).ConfigureAwait(false);

        return new HtmlDocument(browsingContext, document);
    }

    private static async Task<IHtmlDocument> OpenRemoteHtmlDocumentInternalAsync(Uri htmlUri)
    {
        var browsingContext = CreateBrowsingContext();
        var document = await browsingContext.OpenAsync(htmlUri.AbsoluteUri).ConfigureAwait(false);

        return new HtmlDocument(browsingContext, document);
    }

    private IEnumerable<IResource> CollectAudios(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        foreach (var audioNode in angleSharpDocument.GetElementsByTagName("audio").Cast<IHtmlAudioElement>())
        {
            var audioNodeWrapper = new AudioNodeWrapper(audioNode);

            if (!audioNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!audioNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Audio source URI could not be parsed: {audioNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(audioNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectEmbeddedStyleSheets(IDocument angleSharpDocument, CollectionCounts counts)
    {
        var embeddedStyleSheets = angleSharpDocument
            .GetElementsByTagName("style")
            .Cast<IHtmlStyleElement>()
            .Where(s => string.IsNullOrWhiteSpace(s.Type) || string.Equals(s.Type, MimeTypeMap.TEXT.CSS, StringComparison.OrdinalIgnoreCase));

        foreach (var styleNode in embeddedStyleSheets)
        {
            var styleNodeWrapper = new StyleNodeWrapper(styleNode);

            if (!styleNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }

            ++counts.Collected;
            yield return new EmbeddedResource(styleNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectExternalStyleSheets(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        var externalStyleSheets = angleSharpDocument
            .GetElementsByTagName("link")
            .Cast<IHtmlLinkElement>()
            .Where(l => string.Equals(l.Relation, "stylesheet", StringComparison.OrdinalIgnoreCase));

        foreach (var linkNode in externalStyleSheets)
        {
            var linkNodeWrapper = new LinkNodeWrapper(linkNode);

            if (!linkNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!linkNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Link source URI could not be parsed: {linkNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(linkNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectFavicons(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        var favicons = angleSharpDocument
            .GetElementsByTagName("link")
            .Cast<IHtmlLinkElement>()
            .Where(l => string.Equals(l.Relation, "icon", StringComparison.OrdinalIgnoreCase) ||
                        string.Equals(l.Relation, "apple-touch-icon", StringComparison.OrdinalIgnoreCase));

        foreach (var linkNode in favicons)
        {
            var linkNodeWrapper = new LinkNodeWrapper(linkNode);

            if (!linkNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!linkNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Link source URI could not be parsed: {linkNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(linkNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectImages(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        foreach (var imageNode in angleSharpDocument.Images)
        {
            var imageNodeWrapper = new ImageNodeWrapper(imageNode);

            if (!imageNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!imageNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Image source URI could not be parsed: {imageNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(imageNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectImageSourceSets(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        var imageCandidateNodes = angleSharpDocument.Images
            .Where(x => !string.IsNullOrWhiteSpace(x.SourceSet))
            .Select(x => new { ImageNode = x, ImageCandidates = SourceSet.Parse(x.SourceSet!) });

        foreach (var imageCandidateNode in imageCandidateNodes)
        {
            foreach (var imageCandidate in imageCandidateNode.ImageCandidates)
            {
                var imageCandidateNodeWrapper = new ImageCandidateNodeWrapper(imageCandidateNode.ImageNode, imageCandidate);

                if (!imageCandidateNodeWrapper.CanBeCollected(_logger))
                {
                    ++counts.Skipped;
                    continue;
                }
                if (!imageCandidateNodeWrapper.TryParseRawSourceUri(_logger))
                {
                    if (ignoreErrors)
                    {
                        ++counts.Skipped;
                        continue;
                    }
                    throw new InvalidOperationException($"Image candidate source URI could not be parsed: {imageCandidateNodeWrapper.RawSourceUri}");
                }

                ++counts.Collected;
                yield return new ExternalResource(imageCandidateNodeWrapper);
            }
        }
    }

    private IEnumerable<IResource> CollectPictureSources(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        foreach (var pictureSourceNode in angleSharpDocument.Images.SelectMany(x => x.GetSources()))
        {
            var pictureSourceNodeWrapper = new SourceNodeWrapper(pictureSourceNode, AttributeNames.SourceSet);

            if (!pictureSourceNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!pictureSourceNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Picture source URI could not be parsed: {pictureSourceNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(pictureSourceNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectScripts(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        var scripts = angleSharpDocument
            .GetElementsByTagName("script")
            .Cast<IHtmlScriptElement>()
            .Where(s => !string.IsNullOrWhiteSpace(s.Source));

        foreach (var scriptNode in scripts)
        {
            var scriptNodeWrapper = new ScriptNodeWrapper(scriptNode);

            if (!scriptNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!scriptNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Script source URI could not be parsed: {scriptNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(scriptNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectVideoPosters(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        foreach (var videoNode in angleSharpDocument.GetElementsByTagName("video").Cast<IHtmlVideoElement>())
        {
            var videoNodeWrapper = new VideoNodeWrapper(videoNode, "poster");

            if (!videoNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!videoNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Video poster URI could not be parsed: {videoNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(videoNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectVideos(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        foreach (var videoNode in angleSharpDocument.GetElementsByTagName("video").Cast<IHtmlVideoElement>())
        {
            var videoNodeWrapper = new VideoNodeWrapper(videoNode, AttributeNames.Source);

            if (!videoNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!videoNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Video source URI could not be parsed: {videoNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(videoNodeWrapper);
        }
    }

    private IEnumerable<IResource> CollectVideoSources(bool ignoreErrors, IDocument angleSharpDocument, CollectionCounts counts)
    {
        foreach (var videoSourceNode in angleSharpDocument.GetElementsByTagName("video").Children(">source").Cast<IHtmlSourceElement>())
        {
            var videoSourceNodeWrapper = new SourceNodeWrapper(videoSourceNode, AttributeNames.Source);

            if (!videoSourceNodeWrapper.CanBeCollected(_logger))
            {
                ++counts.Skipped;
                continue;
            }
            if (!videoSourceNodeWrapper.TryParseRawSourceUri(_logger))
            {
                if (ignoreErrors)
                {
                    ++counts.Skipped;
                    continue;
                }
                throw new InvalidOperationException($"Video source URI could not be parsed: {videoSourceNodeWrapper.RawSourceUri}");
            }

            ++counts.Collected;
            yield return new ExternalResource(videoSourceNodeWrapper);
        }
    }

    private IEnumerable<IResource> LogCollectionCounts(string collectionName, CollectionCounts counts)
    {
        _logger.LogTrace("{CollectedVideoCount} {CollectionName} have been collected and {SkippedVideoCount} have been skipped", counts.Collected, collectionName, counts.Skipped);
        return Enumerable.Empty<IResource>();
    }

    private sealed class CollectionCounts
    {
        public int Collected { get; set; }

        public int Skipped { get; set; }
    }

    private sealed class HtmlDocument : IHtmlDocument
    {
        public HtmlDocument(IBrowsingContext browsingContext, IDocument document)
        {
            BrowsingContext = browsingContext;
            Document = document;
        }

        public IBrowsingContext BrowsingContext { get; }

        public IDocument Document { get; }

        public Uri DocumentUri => new(Document.DocumentUri);

        public void Dispose()
        {
            BrowsingContext?.Dispose();
            Document?.Dispose();
        }
    }
}
