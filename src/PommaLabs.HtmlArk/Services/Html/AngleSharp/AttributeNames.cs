﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal static class AttributeNames
{
    public const string Href = "href";
    public const string Poster = "poster";
    public const string Source = "src";
    public const string SourceSet = "srcset";
}
