﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using AngleSharp.Html.Dom;
using static AngleSharp.Html.SourceSet;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class ImageCandidateNodeWrapper : HrefNodeWrapper<IHtmlImageElement>
{
    private readonly ImageCandidate _imageCandidate;

    public ImageCandidateNodeWrapper(IHtmlImageElement wrappedImageNode, ImageCandidate imageCandidate)
        : base(wrappedImageNode, AttributeNames.SourceSet, imageCandidate.Url)
    {
        _imageCandidate = imageCandidate;
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the image data URI. Image candidate URL is not null because that was
        // already verified in CanBeCollected method.
        WrappedHtmlNode.SourceSet = WrappedHtmlNode.SourceSet?.Replace(_imageCandidate.Url!, resourceContents);
    }
}
