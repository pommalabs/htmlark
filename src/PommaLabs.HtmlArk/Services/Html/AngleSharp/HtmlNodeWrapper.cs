﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp.Css.Dom;
using AngleSharp.Css.Parser;
using AngleSharp.Css.Values;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using PommaLabs.HtmlArk.Core;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

/// <summary>
///   Wraps an HTML node.
/// </summary>
internal abstract class HtmlNodeWrapper : IHtmlNodeWrapper
{
    /// <summary>
    ///   Determines whether the node can be collected or not.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <returns>Whether the node can be collected or not.</returns>
    public abstract bool CanBeCollected(ILogger logger);

    /// <summary>
    ///   Embeds specified contents into the wrapped HTML node.
    /// </summary>
    /// <param name="resourceContents">Resource contents.</param>
    public abstract void EmbedContents(string resourceContents);

    /// <summary>
    ///   Collects the resource URIs nested in CSS.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="settings">Settings.</param>
    /// <param name="resourceContents">Resource contents.</param>
    /// <returns>Collected resource URIs.</returns>
    protected static HashSet<Uri> CollectNestedCssResourceUris(ILogger logger, HtmlArchiverSettings settings, string resourceContents)
    {
        var cssParser = new CssParser();
        var styleSheet = cssParser.ParseStyleSheet(resourceContents);

        var backgroundImages = Enumerable.Empty<Uri>();
        if (settings.IgnoreImages)
        {
            logger.LogTrace("Ignore images flag is set to true, no image nested in CSS will be collected");
        }
        else
        {
            backgroundImages = styleSheet.Rules
                .Select(r => r as ICssStyleRule)
                .Where(r => r != null)
                .Select(r => r!.Style)
                .SelectMany(r => r)
                .Select(p => p.RawValue as CssListValue<ICssValue>)
                .Where(p => p != null)
                .SelectMany(p => p!)
                .Select(v => v.AsUrl())
                .Where(u => u != null && !UriHelper.IsDataScheme(u))
                .Select(u => new Uri(u, UriKind.RelativeOrAbsolute));
        }

        var fonts = Enumerable.Empty<Uri>();
        if (settings.IgnoreFonts)
        {
            logger.LogTrace("Ignore fonts flag is set to true, no font declared CSS will be collected");
        }
        else
        {
            fonts = styleSheet.Rules
                .Select(r => r as ICssFontFaceRule)
                .Where(r => r != null)
                .SelectMany(r => r!)
                .Where(p => p.Name == "src")
                .Select(p => p.RawValue as CssListValue<ICssValue>)
                .Where(p => p != null)
                .SelectMany(p => p!)
                .Select(v => v as CssTupleValue<ICssValue>)
                .Where(v => v != null)
                .SelectMany(v => v!)
                .Select(v => v.AsUrl())
                .Where(u => u != null && !UriHelper.IsDataScheme(u))
                .Select(u => new Uri(u, UriKind.RelativeOrAbsolute));
        }

        return new HashSet<Uri>(backgroundImages.Union(fonts));
    }
}

internal abstract class HtmlNodeWrapper<TNode> : HtmlNodeWrapper
    where TNode : IHtmlElement
{
    protected HtmlNodeWrapper(TNode wrappedHtmlNode)
    {
        WrappedHtmlNode = wrappedHtmlNode;
    }

    protected TNode WrappedHtmlNode { get; }
}
