﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using PommaLabs.MimeTypes;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class StyleNodeWrapper : HtmlNodeWrapper<IHtmlStyleElement>, ITextNodeWrapper
{
    public StyleNodeWrapper(IHtmlStyleElement wrappedStyleNode) : base(wrappedStyleNode)
    {
    }

    public string Contents => WrappedHtmlNode.InnerHtml;

    public string ContentType => string.IsNullOrWhiteSpace(WrappedHtmlNode.Type) ? MimeTypeMap.TEXT.CSS : WrappedHtmlNode.Type!;

    public override bool CanBeCollected(ILogger logger)
    {
        return true;
    }

    public HashSet<Uri> CollectNestedResourceUris(ILogger logger, HtmlArchiverSettings settings, string resourceContents)
    {
        return CollectNestedCssResourceUris(logger, settings, resourceContents);
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the updated style sheet definition.
        WrappedHtmlNode.InnerHtml = resourceContents;
    }
}
