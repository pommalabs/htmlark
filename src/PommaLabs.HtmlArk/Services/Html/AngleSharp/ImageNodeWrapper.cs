﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using AngleSharp.Html.Dom;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class ImageNodeWrapper : HrefNodeWrapper<IHtmlImageElement>
{
    public ImageNodeWrapper(IHtmlImageElement wrappedImageNode)
        : base(wrappedImageNode, AttributeNames.Source, wrappedImageNode.Source)
    {
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the image data URI.
        WrappedHtmlNode.Source = resourceContents;
    }
}
