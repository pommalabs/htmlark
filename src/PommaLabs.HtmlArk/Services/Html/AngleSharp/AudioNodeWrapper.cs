﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using AngleSharp.Html.Dom;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class AudioNodeWrapper : HrefNodeWrapper<IHtmlAudioElement>
{
    public AudioNodeWrapper(IHtmlAudioElement wrappedAudioNode)
        : base(wrappedAudioNode, AttributeNames.Source, wrappedAudioNode.Source)
    {
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the audio data URI.
        WrappedHtmlNode.Source = resourceContents;
    }
}
