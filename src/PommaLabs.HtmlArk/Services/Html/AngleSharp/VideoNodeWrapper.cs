﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using AngleSharp.Html.Dom;
using PommaLabs.HtmlArk.Resources;

namespace PommaLabs.HtmlArk.Services.Html.AngleSharp;

internal sealed class VideoNodeWrapper : HrefNodeWrapper<IHtmlVideoElement>
{
    public VideoNodeWrapper(IHtmlVideoElement wrappedVideoNode, string attributeName)
        : base(wrappedVideoNode, attributeName, GetRawSourceUri(wrappedVideoNode, attributeName))
    {
    }

    public override void EmbedContents(string resourceContents)
    {
        // Here we receive the video or the poster data URI.
        switch (AttributeName)
        {
            case AttributeNames.Source:
                WrappedHtmlNode.Source = resourceContents;
                break;

            case AttributeNames.Poster:
                WrappedHtmlNode.Poster = resourceContents;
                break;
        }
    }

    private static string? GetRawSourceUri(IHtmlVideoElement wrappedVideoNode, string attributeName)
    {
        return attributeName switch
        {
            AttributeNames.Source => wrappedVideoNode.Source,
            AttributeNames.Poster => wrappedVideoNode.Poster,
            _ => throw new ArgumentException(ErrorMessages.HtmlNodeWrapper_UnknownAttributeName, nameof(attributeName))
        };
    }
}
