﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading.Tasks;

namespace PommaLabs.HtmlArk.Services.File;

/// <summary>
///   Interface for file system access.
/// </summary>
public interface IFileService
{
    /// <summary>
    ///   Determines whether the specified file exists.
    /// </summary>
    /// <param name="path">The file to check.</param>
    /// <returns>
    ///   <c>true</c> if the caller has the required permissions and path contains the name of
    ///   an existing file; otherwise, <c>false</c>.
    /// </returns>
    Task<bool> FileExistsAsync(string path);

    /// <summary>
    ///   Opens a text file, reads all lines of the file, and then closes the file.
    /// </summary>
    /// <param name="path">The file to open for reading.</param>
    /// <returns>A string containing all lines of the file.</returns>
    Task<string> ReadTextFileAsync(string path);
}
