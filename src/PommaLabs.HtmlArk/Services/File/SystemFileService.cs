﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Threading.Tasks;
using SystemFile = System.IO.File;

namespace PommaLabs.HtmlArk.Services.File;

/// <summary>
///   File service which talks with file system.
/// </summary>
public sealed class SystemFileService : IFileService
{
    /// <inheritdoc/>
    public Task<bool> FileExistsAsync(string path)
    {
        return Task.FromResult(SystemFile.Exists(path));
    }

    /// <inheritdoc/>
    public Task<string> ReadTextFileAsync(string path)
    {
        return Task.FromResult(SystemFile.ReadAllText(path).NormalizeLineEndings());
    }
}
