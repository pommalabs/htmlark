﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.HtmlArk.Resources;

internal static class ErrorMessages
{
    public const string HtmlArchiver_HtmlFileNotFound = "Specified HTML was not found";
    public const string HtmlArchiver_HtmlPathNullOrWhiteSpace = "Specified HTML path is null, empty or blank";
    public const string HtmlArchiver_UnreadableExternalResource = "External resource could not be read";

    public const string HtmlService_HtmlContentsNullOrWhiteSpace = "Specified HTML contents are null, empty or blank";
    public const string HtmlService_HtmlDocumentWithUnexpectedType = "HTML document instance has an unexpected type";
    public const string HtmlService_HtmlUriIsFile = "Specified HTML URI is a file URI";

    public const string HtmlNodeWrapper_UnknownAttributeName = "Specified attribute name is unknown";
}
