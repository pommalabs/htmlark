﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Threading.Tasks;

namespace PommaLabs.HtmlArk;

/// <summary>
///   Describes the operations available on the HTML archiver.
/// </summary>
public interface IHtmlArchiver
{
    /// <summary>
    ///   Archives the HTML found at <paramref name="htmlUri"/>, using default settings.
    /// </summary>
    /// <param name="htmlUri">Input HTML URI or HTML file path.</param>
    /// <returns>The archival-ready HTML.</returns>
    /// <exception cref="FileNotFoundException">
    ///   Specified <paramref name="htmlUri"/> is a local file and it cannot not be found.
    /// </exception>
    Task<string> ArchiveAsync(Uri htmlUri);

    /// <summary>
    ///   Archives the HTML found at <paramref name="htmlUri"/>, using specified <paramref name="settings"/>.
    /// </summary>
    /// <param name="htmlUri">Input HTML URI or HTML file path.</param>
    /// <param name="settings">Custom settings.</param>
    /// <returns>The archival-ready HTML.</returns>
    /// <exception cref="FileNotFoundException">
    ///   Specified <paramref name="htmlUri"/> is a local file and it cannot not be found.
    /// </exception>
    Task<string> ArchiveAsync(Uri htmlUri, HtmlArchiverSettings settings);

    /// <summary>
    ///   Archives the HTML found at <paramref name="htmlPath"/>, using default settings.
    /// </summary>
    /// <param name="htmlPath">Input HTML file path.</param>
    /// <returns>The archival-ready HTML.</returns>
    /// <exception cref="FileNotFoundException">
    ///   Specified <paramref name="htmlPath"/> cannot not be found.
    /// </exception>
    Task<string> ArchiveAsync(string htmlPath);

    /// <summary>
    ///   Archives the HTML found at <paramref name="htmlPath"/>, using specified <paramref name="settings"/>.
    /// </summary>
    /// <param name="htmlPath">Input HTML file path.</param>
    /// <param name="settings">Custom settings.</param>
    /// <returns>The archival-ready HTML.</returns>
    /// <exception cref="FileNotFoundException">
    ///   Specified <paramref name="htmlPath"/> cannot not be found.
    /// </exception>
    Task<string> ArchiveAsync(string htmlPath, HtmlArchiverSettings settings);
}
