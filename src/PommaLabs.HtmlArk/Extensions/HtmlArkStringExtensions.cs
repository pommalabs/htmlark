﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text;

namespace System;

internal static class HtmlArkStringExtensions
{
    public static string NormalizeLineEndings(this string str)
    {
        // Line endings should be normalized to LF on every OS,
        // otherwise we might get different outputs starting from the same document.
        return str.Replace("\r\n", "\n").Replace("\r", "\n");
    }

    public static string EscapeDataString(this string str)
    {
        const int SliceLength = 50000;

        var escaped = new StringBuilder();
        var idx = 0;

        do
        {
            var slice = str.Substring(idx, Math.Min(SliceLength, str.Length - idx));
            escaped.Append(Uri.EscapeDataString(slice.NormalizeLineEndings()));
            idx += slice.Length;
        } while (idx < str.Length);

        return escaped.ToString();
    }
}
