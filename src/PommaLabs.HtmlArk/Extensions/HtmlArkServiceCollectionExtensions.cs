﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PommaLabs.HtmlArk;
using PommaLabs.HtmlArk.Services.File;
using PommaLabs.HtmlArk.Services.Html;
using PommaLabs.HtmlArk.Services.Html.AngleSharp;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations of <see cref="IHtmlArchiver"/> implementations.
/// </summary>
public static class HtmlArkServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="HtmlArchiver"/> as singleton implementation of <see cref="IHtmlArchiver"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified service collection.</returns>
    public static IServiceCollection AddHtmlArchiver(this IServiceCollection services)
    {
        // Preconditions
        if (services == null) throw new ArgumentNullException(nameof(services));

        services.TryAddSingleton<IFileService, SystemFileService>();
        services.TryAddSingleton<IHtmlService, AngleSharpHtmlService>();
        services.TryAddSingleton<IHtmlArchiver, HtmlArchiver>();

        return services;
    }
}
