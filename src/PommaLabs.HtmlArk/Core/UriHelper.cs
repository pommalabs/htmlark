﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Text;
using PommaLabs.MimeTypes;

namespace PommaLabs.HtmlArk.Core;

internal static class UriHelper
{
    private static readonly Uri s_dummyBaseUri = new("https://base");

    public static string BuildDataUri(byte[] contents, string contentType, string charSet)
    {
        var dataUri = new StringBuilder("data:");

        dataUri.Append(contentType);
        dataUri.Append(';');

        if (charSet == "UTF-8" || MimeTypeMap.GetEncoding(contentType, throwIfMissing: false) != MimeEncoding.Base64)
        {
            dataUri.Append("charset=UTF-8,");
            dataUri.Append(Encoding.UTF8.GetString(contents).EscapeDataString());
        }
        else
        {
            dataUri.Append("base64,");
            dataUri.Append(Convert.ToBase64String(contents));
        }

        return dataUri.ToString();
    }

    public static string GetFileName(Uri uri)
    {
        if (!uri.IsAbsoluteUri)
        {
            uri = new Uri(s_dummyBaseUri, uri);
        }
        return Path.GetFileName(uri.LocalPath);
    }

    public static bool IsDataScheme(string uri) => uri.StartsWith("data:", StringComparison.OrdinalIgnoreCase);

    public static bool IsHttpScheme(Uri uri) => uri.Scheme == "http" || uri.Scheme == "https";

    public static string UnescapeDataString(string str)
    {
        return Uri.UnescapeDataString(str);
    }
}
