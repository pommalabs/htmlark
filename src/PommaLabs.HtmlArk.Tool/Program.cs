﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Threading.Tasks;
using CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace PommaLabs.HtmlArk.Tool;

/// <summary>
///   Tool entry point.
/// </summary>
public static class Program
{
    private static int s_exitCode;

    /// <summary>
    ///   Tool entry point.
    /// </summary>
    /// <param name="args">Command-line arguments.</param>
    public static async Task<int> Main(string[] args)
    {
        await Parser.Default
            .ParseArguments<Options>(args)
            .WithParsedAsync(async opts =>
            {
                await using var serviceProvider = ConfigureServices(opts);
                var logger = serviceProvider.GetRequiredService<ILogger<HtmlArchiver>>();
                var htmlArchiver = serviceProvider.GetRequiredService<IHtmlArchiver>();

                try
                {
                    var (inputUri, outputUri) = ValidateOptions(opts, logger);

                    var archivedHtml = await htmlArchiver.ArchiveAsync(inputUri, new HtmlArchiverSettings
                    {
                        HttpClientMaxResourceSizeInBytes = opts.HttpClientMaxResourceSizeInBytes,
                        HttpClientTimeout = opts.HttpClientTimeout,
                        IgnoreAudios = opts.IgnoreAudios,
                        IgnoreCss = opts.IgnoreCss,
                        IgnoreErrors = opts.IgnoreErrors,
                        IgnoreFonts = opts.IgnoreFonts,
                        IgnoreImages = opts.IgnoreImages,
                        IgnoreJs = opts.IgnoreJs,
                        Minify = opts.Minify,
                    }).ConfigureAwait(false);

                    await WriteArchivedHtmlAsync(archivedHtml, outputUri, logger).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    logger.LogCritical(ex, "A critical error occurred");
                    s_exitCode = 1;
                }
            })
            .ConfigureAwait(false);

        return s_exitCode;
    }

    private static ServiceProvider ConfigureServices(Options opts)
    {
        var services = new ServiceCollection();

        services.AddLogging(options => options.AddConsole().SetMinimumLevel(opts.Verbose ? LogLevel.Trace : LogLevel.Information));
        services.AddSingleton<IHtmlArchiver, HtmlArchiver>();

        return services.BuildServiceProvider();
    }

    private static (Uri inputUri, Uri? outputUri) ValidateOptions(Options opts, ILogger<HtmlArchiver> logger)
    {
        // We need to transform input URI into a valid URI.
        if (!Uri.TryCreate(opts.HtmlUri, UriKind.Absolute, out var inputUri) &&
            !Uri.TryCreate(Path.GetFullPath(opts.HtmlUri), UriKind.Absolute, out inputUri))
        {
            throw new ArgumentException($"Specified input HTML URI or HTML file path is not valid: {opts.HtmlUri}", nameof(opts));
        }

        // If output path has been specified, then it must be a local file.
        Uri? outputUri = default;
        if (opts.OutputPath != null && !Uri.TryCreate(Path.GetFullPath(opts.OutputPath), UriKind.Absolute, out outputUri))
        {
            throw new ArgumentException($"Specified output HTML file path is not valid: {opts.OutputPath}", nameof(opts));
        }

        // Log "--ignore-*" flags, it might be helpful to know which flags were enabled in order
        // to better understand strange issues and problems:
        if (opts.IgnoreErrors)
        {
            logger.LogTrace("\"--ignore-errors\" flag has been specified, unreadable resources will be ignored.");
        }
        if (opts.IgnoreImages)
        {
            logger.LogTrace("\"--ignore-images\" flag has been specified, images will be ignored during archival.");
        }
        if (opts.IgnoreCss)
        {
            logger.LogTrace("\"--ignore-css\" flag has been specified, style sheets will be ignored during archival.");
        }
        if (opts.IgnoreJs)
        {
            logger.LogTrace("\"--ignore-js\" flag has been specified, external JavaScript will be ignored during archival.");
        }
        if (opts.IgnoreFonts)
        {
            logger.LogTrace("\"--ignore-fonts\" flag has been specified, fonts will be ignored during archival.");
        }
        if (opts.IgnoreAudios)
        {
            logger.LogTrace("\"--ignore-audios\" flag has been specified, audios will be ignored during archival.");
        }
        if (opts.IgnoreVideos)
        {
            logger.LogTrace("\"--ignore-videos\" flag has been specified, videos will be ignored during archival.");
        }

        return (inputUri, outputUri);
    }

    private static async Task WriteArchivedHtmlAsync(string archivedHtml, Uri? outputUri, ILogger<HtmlArchiver> logger)
    {
        if (outputUri == null)
        {
            logger.LogTrace("Output file path has not been specified, archived HTML will be written to console output");
            Console.WriteLine(archivedHtml);
            return;
        }

        var outputFilePath = outputUri.LocalPath;
        logger.LogTrace("Archived HTML will be written to specified output file path: {OutputFilePath}", outputFilePath);
        await File.WriteAllTextAsync(outputFilePath, archivedHtml).ConfigureAwait(false);
    }
}
