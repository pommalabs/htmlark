﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics.CodeAnalysis;
using CommandLine;

namespace PommaLabs.HtmlArk.Tool;

[SuppressMessage("Performance", "CA1812:Avoid uninstantiated internal classes", Justification = "Used by command-line parser")]
internal sealed class Options
{
    [Value(0, Required = true, MetaName = "input", HelpText = "Input HTML URI or HTML file path.")]
    public string HtmlUri { get; set; } = string.Empty;

    [Option('M', "http-client-max-resource-size", Required = false, HelpText = "How many bytes can be downloaded for each resource.")]
    public long HttpClientMaxResourceSizeInBytes { get; set; } = HtmlArchiverSettings.Default.HttpClientMaxResourceSizeInBytes;

    [Option('T', "http-client-timeout", Required = false, HelpText = "Timeout of the internal HTTP client.")]
    public TimeSpan HttpClientTimeout { get; set; } = HtmlArchiverSettings.Default.HttpClientTimeout;

    [Option('A', "ignore-audios", Required = false, HelpText = "Ignores audios during archival.")]
    public bool IgnoreAudios { get; set; } = HtmlArchiverSettings.Default.IgnoreAudios;

    [Option('C', "ignore-css", Required = false, HelpText = "Ignores style sheets during archival.")]
    public bool IgnoreCss { get; set; } = HtmlArchiverSettings.Default.IgnoreCss;

    [Option('E', "ignore-errors", Required = false, HelpText = "Ignores unreadable resources.")]
    public bool IgnoreErrors { get; set; } = HtmlArchiverSettings.Default.IgnoreErrors;

    [Option('F', "ignore-fonts", Required = false, HelpText = "Ignores fonts during archival.")]
    public bool IgnoreFonts { get; set; } = HtmlArchiverSettings.Default.IgnoreFonts;

    [Option('I', "ignore-images", Required = false, HelpText = "Ignores images during archival.")]
    public bool IgnoreImages { get; set; } = HtmlArchiverSettings.Default.IgnoreImages;

    [Option('J', "ignore-js", Required = false, HelpText = "Ignores external JavaScript during archival.")]
    public bool IgnoreJs { get; set; } = HtmlArchiverSettings.Default.IgnoreJs;

    [Option('V', "ignore-videos", Required = false, HelpText = "Ignores videos during archival.")]
    public bool IgnoreVideos { get; set; } = HtmlArchiverSettings.Default.IgnoreVideos;

    [Option('m', "minify", Required = false, HelpText = "Minifies output HTML.")]
    public bool Minify { get; set; } = HtmlArchiverSettings.Default.Minify;

    [Option('o', "output", Required = false, HelpText = "Output HTML file path. If not specified, output will be written to STDOUT.")]
    public string? OutputPath { get; set; }

    [Option('v', "verbose", Required = false, HelpText = "Prints detailed information during HTML archival.")]
    public bool Verbose { get; set; }
}
