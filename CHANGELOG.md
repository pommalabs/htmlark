# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.0] - 2023-11-15

### Changed

- Updated tool to .NET 8.

## [1.5.0] - 2023-05-06

### Changed

- A meta element with "Content-Security-Policy" http-equiv and content `default-src 'unsafe-inline' data:`
  is automatically added to archived pages. If such meta element already exists, then it is replaced.
  That policy ensures that no remote resource is downloaded.

## [1.4.0] - 2023-04-28

### Added

- Restored support for .NET Framework by targeting .NET Standard 2.0.

### Changed

- Line endings of embedded resources are normalized to LF.

## [1.3.0] - 2022-12-17

### Added

- Added support for .NET 7.0.

### Removed

- Dropped support for .NET Core 3.1 and .NET Framework 4.7.2.

## [1.2.0] - 2021-11-21

### Changed

- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.6.1 and 4.7.2.
- The project will target only .NET LTS releases and supported .NET Framework 4.x releases.

## [1.1.0] - 2021-08-13

### Added

- Picture source elements and image source sets are embedded.

## [1.0.0] - 2021-08-12

### Added

- Unit and integration tests.
- Source elements inside video tags are embedded.

## [0.2.0] - 2021-04-25

### Added

- HtmlArk was not embedding CSS background images. Now it does.

### Fixed

- Local fonts were not embedded, it worked only with remote ones.
- Output folders with blank characters were not handled correctly.

## [0.1.1] - 2020-11-15

### Added

- Added support for .NET Core 3.1 LTS to HtmlArk tool.

## [0.1.0] - 2020-11-15

### Added

- Initial release.

[1.6.0]: https://gitlab.com/pommalabs/htmlark/-/compare/1.5.0...1.6.0
[1.5.0]: https://gitlab.com/pommalabs/htmlark/-/compare/1.4.0...1.5.0
[1.4.0]: https://gitlab.com/pommalabs/htmlark/-/compare/1.3.0...1.4.0
[1.3.0]: https://gitlab.com/pommalabs/htmlark/-/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/pommalabs/htmlark/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/pommalabs/htmlark/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/pommalabs/htmlark/-/compare/0.2.0...1.0.0
[0.2.0]: https://gitlab.com/pommalabs/htmlark/-/compare/0.1.1...0.2.0
[0.1.1]: https://gitlab.com/pommalabs/htmlark/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/pommalabs/htmlark/-/tags/0.1.0
