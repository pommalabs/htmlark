﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace PommaLabs.HtmlArk.UnitTests.Extensions;

[TestFixture, Parallelizable]
public sealed class HtmlArkServiceCollectionExtensionsTests
{
    [Test]
    public void AddHtmlArchiver_CalledTwice_ShouldNotThrow()
    {
        // Arrange
        var services = A.Fake<IServiceCollection>();
        services.AddHtmlArchiver();

        // Act & Assert
        Assert.That(() => services.AddHtmlArchiver(), Throws.Nothing);
    }

    [Test]
    public void AddHtmlArchiver_NullServiceCollection_ShouldThrowArgumentNullException()
    {
        // Arrange
        IServiceCollection services = null!;

        // Act & Assert
        Assert.That(() => services.AddHtmlArchiver(), Throws.ArgumentNullException);
    }

    [Test]
    public void AddHtmlArchiver_ValidServiceCollection_ShouldCallServiceCollectionMethods()
    {
        // Arrange
        var services = A.Fake<IServiceCollection>();

        // Act
        services.AddHtmlArchiver();

        // Assert
        A.CallTo(services).MustHaveHappened();
    }

    [Test]
    public void AddHtmlArchiver_ValidServiceCollection_ShouldReturnSameServiceCollectionInstance()
    {
        // Arrange
        var services = A.Fake<IServiceCollection>();

        // Act
        var result = services.AddHtmlArchiver();

        // Assert
        Assert.That(result, Is.SameAs(services));
    }
}
