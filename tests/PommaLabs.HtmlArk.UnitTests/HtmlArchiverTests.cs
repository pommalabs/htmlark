﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using PommaLabs.HtmlArk.Services.File;
using PommaLabs.HtmlArk.Services.Html;

namespace PommaLabs.HtmlArk.UnitTests;

[TestFixture, Parallelizable]
public sealed class HtmlArchiverTests
{
    private readonly string _fakeHtmlPath = "a.html";
    private readonly Uri _fakeHtmlUri = new("http://localhost/a.html");

    [Test]
    public async Task LocalPage_ExistingHtmlFile_ShouldCallStripUnnecessaryElements()
    {
        // Arrange
        var fileService = A.Fake<IFileService>();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);

        A.CallTo(() => fileService.FileExistsAsync(A<string>._)).Returns(Task.FromResult(true));

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath);

        // Assert
        A.CallTo(() => htmlService.StripUnnecessaryElements(An<IHtmlDocument>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task LocalPage_ExistingHtmlFile_ShouldCallAppendContentSecurityPolicyMetaElement()
    {
        // Arrange
        var fileService = A.Fake<IFileService>();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);

        A.CallTo(() => fileService.FileExistsAsync(A<string>._)).Returns(Task.FromResult(true));

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath);

        // Assert
        A.CallTo(() => htmlService.AppendContentSecurityPolicyMetaElement(An<IHtmlDocument>._)).MustHaveHappenedOnceExactly();
    }

    [TestCase("")]
    [TestCase(" ")]
    [TestCase("   ")]
    [TestCase("\t ")]
    public void LocalPage_InvalidHtmlPathWithCustomSettings_ShouldThrowArgumentException(string htmlPath)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();
        var settings = A.Dummy<HtmlArchiverSettings>();

        // Act & Assert
        Assert.ThrowsAsync<ArgumentException>(() => htmlArchiver.ArchiveAsync(htmlPath, settings));
    }

    [TestCase("")]
    [TestCase(" ")]
    [TestCase("   ")]
    [TestCase("\t ")]
    public void LocalPage_InvalidHtmlPathWithDefaultSettings_ShouldThrowArgumentException(string htmlPath)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();

        // Act & Assert
        Assert.ThrowsAsync<ArgumentException>(() => htmlArchiver.ArchiveAsync(htmlPath));
    }

    [TestCase("not-existing.html")]
    [TestCase("a/b/c.html")]
    [TestCase("../404.html")]
    public void LocalPage_NotExistingHtmlFile_ShouldThrowFileNotFoundException(string htmlPath)
    {
        // Arrange
        var fileService = A.Fake<IFileService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService);

        A.CallTo(() => fileService.FileExistsAsync(A<string>._)).Returns(Task.FromResult(false));

        // Act & Assert
        Assert.ThrowsAsync<FileNotFoundException>(() => htmlArchiver.ArchiveAsync(htmlPath));
    }

    [Test]
    public async Task RemotePage_ExistingHtmlUri_ShouldCallStripUnnecessaryElements()
    {
        // Arrange
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(htmlService: htmlService);

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlUri);

        // Assert
        A.CallTo(() => htmlService.StripUnnecessaryElements(An<IHtmlDocument>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task RemotePage_ExistingHtmlUri_ShouldCallAppendContentSecurityPolicyMetaElement()
    {
        // Arrange
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(htmlService: htmlService);

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlUri);

        // Assert
        A.CallTo(() => htmlService.AppendContentSecurityPolicyMetaElement(An<IHtmlDocument>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ResourceCollection_IgnoreAudios_ShouldNotCallCollectAllAudios()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreAudios = true };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllAudios(An<IHtmlDocument>._, A<bool>._)).MustNotHaveHappened();
    }

    [Test]
    public async Task ResourceCollection_IgnoreCss_ShouldNotCallCollectAllStyleSheets()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreCss = true };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllStyleSheets(An<IHtmlDocument>._, A<bool>._)).MustNotHaveHappened();
    }

    [Test]
    public async Task ResourceCollection_IgnoreImages_ShouldNotCallCollectAllImages()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreImages = true };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllImages(An<IHtmlDocument>._, A<bool>._)).MustNotHaveHappened();
    }

    [Test]
    public async Task ResourceCollection_IgnoreJs_ShouldNotCallCollectAllScripts()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreJs = true };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllScripts(An<IHtmlDocument>._, A<bool>._)).MustNotHaveHappened();
    }

    [Test]
    public async Task ResourceCollection_IgnoreVideos_ShouldNotCallCollectAllVideos()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreVideos = true };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllVideos(An<IHtmlDocument>._, A<bool>._)).MustNotHaveHappened();
    }

    [Test]
    public async Task ResourceCollection_IncludeAudios_ShouldCallCollectAllAudios()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreAudios = false };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllAudios(An<IHtmlDocument>._, A<bool>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ResourceCollection_IncludeCss_ShouldCallCollectAllStyleSheets()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreCss = false };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllStyleSheets(An<IHtmlDocument>._, A<bool>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ResourceCollection_IncludeImages_ShouldCallCollectAllImages()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreImages = false };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllImages(An<IHtmlDocument>._, A<bool>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ResourceCollection_IncludeJs_ShouldCallCollectAllScripts()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreJs = false };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllScripts(An<IHtmlDocument>._, A<bool>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ResourceCollection_IncludeVideos_ShouldCallCollectAllVideos()
    {
        // Arrange
        var fileService = CreateFileServiceForFakeHtmlPath();
        var htmlService = A.Fake<IHtmlService>();
        var htmlArchiver = new HtmlArchiver(fileService: fileService, htmlService: htmlService);
        var settings = new HtmlArchiverSettings { IgnoreVideos = false };

        // Act
        await htmlArchiver.ArchiveAsync(_fakeHtmlPath, settings);

        // Act & Assert
        A.CallTo(() => htmlService.CollectAllVideos(An<IHtmlDocument>._, A<bool>._)).MustHaveHappenedOnceExactly();
    }

    private IFileService CreateFileServiceForFakeHtmlPath()
    {
        var fileService = A.Fake<IFileService>();

        A.CallTo(() => fileService.FileExistsAsync(A<string>.That.Contains(_fakeHtmlPath))).Returns(Task.FromResult(true));

        return fileService;
    }
}
