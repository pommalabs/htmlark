﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using NUnit.Framework;
using VerifyNUnit;
using VerifyTests;

namespace PommaLabs.HtmlArk.IntegrationTests;

[TestFixture, Parallelizable]
public sealed class HtmlArchiverTests
{
    private const string HtmlExtension = "html";

    private static readonly string s_assemblyLocation = Path.GetDirectoryName(typeof(HtmlArchiverTests).Assembly.Location)!;

    static HtmlArchiverTests()
    {
        VerifyAngleSharpDiffing.Initialize();
    }

    [TestCase("SamplePages/nothing to do/01.html")]
    public async Task LocalPage_WithAlreadyEmbeddedFiles_ShouldBeArchivedWithoutErrors(string localPage)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();
        var samplePagePath = Path.Combine(s_assemblyLocation, localPage);

        // Act
        var archivedHtml = await htmlArchiver.ArchiveAsync(new Uri(samplePagePath));

        // Assert
        await Verifier.Verify(archivedHtml, HtmlExtension, CreateVerifySettings(localPage));
    }

    [TestCase("SamplePages/local/Alessio Parma.html")]
    [TestCase("SamplePages/local/April Template.html")]
    [TestCase("SamplePages/audio-video/local.html")]
    [TestCase("SamplePages/directory with blanks 01/index.html")]
    [TestCase("SamplePages/srcset/img.html")]
    public async Task LocalPage_WithLocalFiles_ShouldBeArchivedWithoutErrors(string localPage)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();
        var samplePagePath = Path.Combine(s_assemblyLocation, localPage);

        // Act
        var archivedHtml = await htmlArchiver.ArchiveAsync(new Uri(samplePagePath));

        // Assert
        await Verifier.Verify(archivedHtml, HtmlExtension, CreateVerifySettings(localPage));
    }

    [TestCase("SamplePages/local/Alessio Parma.html")]
    [TestCase("SamplePages/local/April Template.html")]
    [TestCase("SamplePages/audio-video/local.html")]
    [TestCase("SamplePages/directory with blanks 01/index.html")]
    [TestCase("SamplePages/srcset/img.html")]
    public async Task LocalPage_WithLocalFilesAndOutputMinificationEnabled_ShouldBeArchivedWithoutErrors(string localPage)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();
        var samplePagePath = Path.Combine(s_assemblyLocation, localPage);

        // Act
        var archivedHtml = await htmlArchiver.ArchiveAsync(new Uri(samplePagePath), new HtmlArchiverSettings
        {
            Minify = true,
        });

        // Assert
        await Verifier.Verify(archivedHtml, HtmlExtension, CreateVerifySettings(localPage));
    }

    [TestCase("SamplePages/local-with-remote-files.html")]
    [TestCase("SamplePages/audio-video/remote.html")]
    public async Task LocalPage_WithRemoteFiles_ShouldBeArchivedWithoutErrors(string localPage)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();
        var samplePagePath = Path.Combine(s_assemblyLocation, localPage);

        // Act
        var archivedHtml = await htmlArchiver.ArchiveAsync(new Uri(samplePagePath));

        // Assert
        await Verifier.Verify(archivedHtml, HtmlExtension, CreateVerifySettings(localPage));
    }

    [TestCase("https://gitlab.com/pommalabs/htmlark/-/raw/main/tests/PommaLabs.HtmlArk.IntegrationTests/SamplePages/april/index.html?inline=false")]
    [TestCase("https://gitlab.com/pommalabs/htmlark/-/raw/main/tests/PommaLabs.HtmlArk.IntegrationTests/SamplePages/solid/index.html?inline=false")]
    public async Task RemotePage_WithRemoteFiles_ShouldBeArchivedWithoutErrors(string remotePage)
    {
        // Arrange
        var htmlArchiver = new HtmlArchiver();

        // Act
        var archivedHtml = await htmlArchiver.ArchiveAsync(new Uri(remotePage));

        // Assert
        await Verifier.Verify(archivedHtml, HtmlExtension, CreateVerifySettings(remotePage));
    }

    private static VerifySettings CreateVerifySettings(string htmlUri, [CallerMemberName] string? methodName = null)
    {
        var settings = new VerifySettings();

        settings.UseDirectory("VerifiedResults");
        settings.UseFileName($"{nameof(HtmlArchiverTests)}.{methodName}.{Farmhash.Sharp.Farmhash.Hash32(htmlUri)}");

        return settings;
    }
}
